const path = require('path');
const webpack = require('webpack');

const BUILD_DIR = path.resolve('..', '..', 'public', 'assets', 'js');
const APP_DIR = path.resolve(__dirname, 'src');

const config = {
    entry: {
        app:path.resolve(APP_DIR, 'index.js'),
},
    output: {
        path: BUILD_DIR,
        filename: '[name].bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.js?/,
                include : APP_DIR,
                use: [
                    'babel-loader',
                    'eslint-loader'
                ]
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            limit: 8192
                        }
                    }
                ]
            },
            {
                test: /\.css$/,
                use: [
                    { loader: 'style-loader' },
                    { loader: 'css-loader' }
                ]
            },
            {
                test: /\.scss$/,
                use: [{
                    loader: "style-loader" // creates style nodes from JS strings
                }, {
                    loader: "css-loader" // translates CSS into CommonJS
                }, {
                    loader: "sass-loader" // compiles Sass to CSS
                }]
            },
            {
                test: /\.(woff|woff2|eot|ttf|svg)$/,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 100000
                    }
                }
            },
            {
                test: /\.json$/,
                use: ['json-loader']
            }
        ]
    },
    resolve: {
        modules:['node_modules', APP_DIR],
        extensions: [' ', '.webpack.js', '.web.js', '.js'],
    },
    devtool: 'sourcemap',
    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        })
    ]
}

module.exports = config;