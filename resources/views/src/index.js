import 'jquery/dist/jquery.js'
import 'bootstrap/dist/js/bootstrap.js';
import 'bootstrap/dist/css/bootstrap.css';
import 'css/index.css';
import 'js/default';
const shopifyAppType = $('meta[name="shopifyAppType"]').attr('content');
const shopifyAppKey = $('meta[name="shopifyAppKey"]').attr('content');
const domain = $('meta[name="domain"]').attr('content');

if (shopifyAppType === 'public') {

    ShopifyApp.init({
        apiKey: shopifyAppKey,
        shopOrigin: 'https://' + domain,
        forceRedirect: true
    });

    ShopifyApp.ready(function() {

    });
}

