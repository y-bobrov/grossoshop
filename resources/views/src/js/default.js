function showObjectInputs(object, outputTag) {

    let properties = Object.keys(object),
        propertiesCount = properties.length,
        values = Object.values(object);

    for (let i = 0; i < propertiesCount; i++) {
        outputTag.find(`input[name=${properties[i]}]`).val(values[i]);
    }

}

function setObjectInputs(object) {

    let properties = Object.keys(object),
        propertiesCount = properties.length,
        values = Object.values(object);

    for (let i = 0; i < propertiesCount; i++) {


        // outputTag.find(`input[name=${properties[i]}]`).val(values[i]);
    }

}


$(document).ready(function () {

    const body = $('body'),
        quickSpecifications = body.find('#quick_spec'),
        designBlock = body.find('#design'),
        designStructure = designBlock.find('#structure'),
        designScreen = designBlock.find('#screen'),
        cameraBlock = body.find('#camera'),
        editProductForm = $('#edit_product_form'),
        editProductModal = $('#edit_modal'),
        uploadImageBtn = $('#upload_images'),
        cameraImageInput = $('#camera_image'),
        photosBlock = $('#photos'),
        videosBlock = $('#videos'),
        imagesInput = $('#images'),
        videosInput = $('#video_input'),
        productsTab = $('#pills-home-tab');

    $('#save_fields').on('submit', function (e) {
        e.preventDefault();

        let data = new FormData();


        $(this).find('input[type=file]').each(function (index, icon) {


            let field = $(icon).parents(':eq(1)').data('field');


            data.append(`icon[${field}]`, icon.files[0]);

        });

        $(this).find('input[type=text]').each(function (index, title) {
            let field = $(title).parents(':eq(1)').data('field');

            data.append(`title[${field}]`, $(title).val());

        });

        //data.append('customer_avatar', this.files[0]);
        $.ajax({
            url: $(this).attr('action'),
            method: "POST",
            data: data,
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                let response = JSON.parse(data);

                if (response.success) {

                    $('#message').show();

                }
            }
        });


    })

    $('#add_field').on('click', function () {

        let newFields = $("#field_template").clone(),
            newFieldId = $('.inputs').find('.field').last().data('field') + 1;

        newFields.removeAttr('id');
        newFields.appendTo('.inputs');
        newFields.data('field', newFieldId);

        $('.inputs').find('.field').show();
    })


    body.on('click', '.delete_field', function () {
        let input = $(this).parents(':eq(1)'),
            field = input.data('field'),
            url = $(this).data('url');

        input.remove();

        $.ajax({
            url: `${url}/${field}`,
            method: "DELETE",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {

            }
        });


    })

    $('.edit_product').on('click', function () {
        let id = $(this).data('id');
        editProductForm.data('id', id);
        $.ajax({
            url: $(this).data('url'),
            method: "GET",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {


                data.quick_specifications.forEach(function (field, i, arr) {

                    quickSpecifications.find(`input[name=${field.title}]`).val(field.pivot.value);

                });
                //setObjectInputs(data.meta);

                let properties = Object.keys(data.meta),
                    propertiesCount = properties.length,
                    values = Object.values(data.meta);

                for (let i = 0; i < propertiesCount; i++) {

                    let tag = body.find(`#${properties[i]}`);
                    showObjectInputs(values[i], tag);

                    // outputTag.find(`input[name=${properties[i]}]`).val(values[i]);
                }

                let array = [];
                photosBlock.html('');
                imagesInput.val('');
                videosBlock.html('');
                videosInput.val('');
                data.photos.forEach(function (photo, i, arr) {


                    array.push(photo.id);
                    imagesInput.val(JSON.stringify(array));


                    let img = `<div class="col-sm-3 text-center">
                                     <button type="button" class="close" aria-label="Close">
                                         <span aria-hidden="true">&times;</span>
                                     </button>
                                     <img class="img-thumbnail" id="${photo.id}" src="${photo.url}" alt="" /></div>`;
                    photosBlock.append(img);


                });


                let videos = [];

                data.videos.forEach(function (video, i, arr) {


                    videos.push(video.id);
                    videosInput.val(JSON.stringify(videos));


                    let vd = `<div class="col-sm-3 text-center">
                                     <button type="button" class="close" aria-label="Close">
                                         <span aria-hidden="true">&times;</span>
                                     </button>
                                     <video width="219px" height="128px" controls>
                                         <source src="${video.url}" type="video/mp4">
                                    </video>
                                   </div>`;

                    videosBlock.append(vd);


                });


                editProductModal.modal('show')

            }
        });


        //$('#edit-modal').modal('show')

    })


    editProductForm.on('submit', function (e) {
        e.preventDefault();
        let url = $(this).attr('action') + "/" + $(this).data('id');

        let data = new FormData();


        editProductForm.find('input').each(function (index, input) {
            let blockName = $(input).parents(':eq(1)').attr('id');

            data.append(`${blockName}[${$(input).attr('name')}]`, $(input).val())

        });


        quickSpecifications.find('input').each(function (index, input) {
            data.append(`quick_specifications[${$(input).attr('name')}]`, $(input).val())

        });

        if (imagesInput.val()) {
            data.append('images', JSON.parse(imagesInput.val()));

        }
        if (videosInput.val()) {
            data.append('videos', JSON.parse(videosInput.val()));

        }


        //data.append('customer_avatar', this.files[0]);
        $.ajax({
            url: url,
            method: "POST",
            data: data,
            contentType: false,
            cache: false,
            processData: false,
            headers:
                {
                    'X-CSRF-TOKEN':
                        $('meta[name="csrf-token"]').attr('content'),
                    "X-HTTP-Method-Override": "PUT"
                }
            ,
            success: function (response) {

                if (response.success) {
                    editProductModal.modal('hide');
                }

            }
        })
        ;


        // $(this).find('input[type=text]').each(function (index, title) {
        //     let field = $(title).parents(':eq(1)').data('field');
        //
        //     data.append(`title[${field}]`, $(title).val());

    });


    $('.upload').on('click', function () {
        let type = $(this).data('type');
        cameraImageInput.data('type', type);
        cameraImageInput.trigger('click');
    })


    cameraImageInput.on('change', function () {


        let url = uploadImageBtn.data('url');
        let data = new FormData();
        let type = $(this).data('type');

        console.log(type);
        console.log(this.files[0].type.includes(type));

        if (this.files[0].type.includes(type)) {


            console.log(this.files[0].type.includes('image'));
            data.append('camera_image', this.files[0]);
            data.append('type', type);
            $.ajax({
                url: url,
                method: "POST",
                data: data,
                contentType: false,
                cache: false,
                processData: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {

                    if (data) {
                        if (!type.localeCompare('image')) {
                            let img = `<div class="col-sm-3 text-center">
                                     <button type="button" class="close" aria-label="Close">
                                         <span aria-hidden="true">&times;</span>
                                     </button>
                                     <img class="img-thumbnail" id="${data.id}" src="${data.url}" alt="" /></div>`;
                            photosBlock.append(img);

                            if (!imagesInput.val()) {
                                let array = [];
                                array.push(data.id);
                                imagesInput.val(JSON.stringify(array));
                            } else {
                                let array = JSON.parse(imagesInput.val());

                                array.push(data.id);
                                imagesInput.val(JSON.stringify(array));
                            }


                        }
                        if (!type.localeCompare('video')) {

                            let video = `<div class="col-sm-3 text-center">
                                     <button type="button" class="close" aria-label="Close">
                                         <span aria-hidden="true">&times;</span>
                                     </button>
                                     <video width="219px" height="128px" controls>
                                         <source src="${data.url}" type="video/mp4">
                                    </video>
                                   </div>`;
                            videosBlock.append(video);
                            if (!videosInput.val()) {
                                let array = [];
                                array.push(data.id);
                                console.log(data.id);
                                videosInput.val(JSON.stringify(array));
                                console.log(videosInput.val());
                            } else {
                                let array = JSON.parse(videosInput.val());

                                array.push(data.id);
                                videosInput.val(JSON.stringify(array));
                            }


                        }

                    }
                }

            })

        }
        else {
            alert('Wrong format');
        }
    });


    body.on('click', '.close', function () {

        $(this).parent().remove();
    })


    productsTab.on('click',function () {
        //let action = $(this).data('action');
        $.ajax({
            url: $(this).data('action'),
            method: "GET",
            contentType: false,
            cache: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {

                $('#quick_spec').html('');
                data.forEach(function (item,i,arr) {

                    let field = `<div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                            <span class="input-group-text logo">
                                                <img class="logo" src="${item.icon}">
                                            </span>
                                                </div>
                                                <input type="text" name="${item.title}" class="form-control"
                                                       aria-label="Amount (to the nearest dollar)">
                                            </div>`;

                    $('#quick_spec').append(field);

                });
                console.log(data);


            }
        });
    })

});
