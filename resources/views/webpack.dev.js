const webpack = require('webpack');
const config = require('./webpack.common');
const path = require('path');

const BUILD_DIR = path.resolve(__dirname, 'public');

config.watch = true;

if (!config.plugins) {
    config.plugins = []
}

config.plugins.push(new webpack.NamedModulesPlugin());
config.plugins.push(new webpack.EnvironmentPlugin([
    "NODE_ENV"
]));

module.exports = config;
