@extends('master')
@section('content')



    <ul class="nav nav-pills nav-fill" id="pills-tab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="pills-home-tab" data-action="{{ action('FieldController@store') }}" data-toggle="pill" href="#products" role="tab"
               aria-controls="pills-home" aria-selected="true">Products</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#settings" role="tab"
               aria-controls="pills-profile" aria-selected="false">Settings</a>
        </li>
    </ul>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="products" role="tabpanel" aria-labelledby="pills-home-tab">
            <table class="table">
                <tbody>
                @foreach($products as $product)

                    <tr>
                        <td class="image">
                            <div class="text-center">
                                <img src="{{$product->image->src}}">

                            </div>
                        </td>
                        <td class="title">
                            <p class="text-center"> {{$product->title}}</p>

                        </td>
                        <td><p class="text-center"> {{$product->body_html}}</p>
                        </td>
                        <td class="buttons">
                            <button
                                    data-url="{{ action('ProductController@show',['id'=>$product->id]) }}"

                                    data-id="{{$product->id}}"
                                    type="button"
                                    class="edit_product btn btn-info align-middle">Edit product
                            </button>

                        </td>
                    </tr>
                @endforeach


                </tbody>
            </table>

            <div id="edit_modal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog"
                 aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Edit product</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <form method="POST"
                              id="edit_product_form"
                              action="{{ action('ProductController@update',['id'=>'']) }}"
                              accept-charset="UTF-8" enctype="multipart/form-data">
                            <div class="modal-body">


                                <div class="row">

                                    <div id="quick_spec" class="col-sm-3">

                                        @foreach($fields as $field)

                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                            <span class="input-group-text logo">
                                                <img class="logo" src="{{$field->icon}}">
                                            </span>
                                                </div>
                                                <input type="text" name="{{$field->title}}" class="form-control"
                                                       aria-label="Amount (to the nearest dollar)">
                                            </div>

                                        @endforeach
                                    </div>
                                    <div id="design" class="col-sm-4">
                                        <div id="structure">
                                            <h6>Design Structure</h6>

                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Size</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="size"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Aspect ratio</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="aspect_ratio"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Weight</span>
                                                </div>
                                                <input type="text"
                                                       name="weight"
                                                       class="form-control" aria-label="Small"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Materials</span>
                                                </div>
                                                <input type="text"
                                                       name="materials"
                                                       class="form-control" aria-label="Small"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Colors</span>
                                                </div>
                                                <input type="text"
                                                       name="colors"
                                                       class="form-control" aria-label="Small"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                        </div>
                                        <div id="screen">
                                            <h6>Screen</h6>

                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Diagonal</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="diagonal"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Type</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="type"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Resolution</span>
                                                </div>
                                                <input type="text"
                                                       name="resolution"
                                                       class="form-control" aria-label="Small"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Density</span>
                                                </div>
                                                <input type="text"
                                                       name="density"
                                                       class="form-control" aria-label="Small"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Others</span>
                                                </div>
                                                <input type="text"
                                                       name="others"
                                                       class="form-control" aria-label="Small"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                        </div>
                                    </div>
                                    <div id="camera" class="col-sm-5">
                                        <h6>Camera</h6>

                                        <div class="input-group input-group-sm mb-3">
                                            <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Resolution</span>
                                            </div>
                                            <input type="text" class="form-control" aria-label="Small"
                                                   name="resolution"
                                                   aria-describedby="inputGroup-sizing-sm">
                                        </div>
                                        <div class="input-group input-group-sm mb-3">
                                            <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Pixel size</span>
                                            </div>
                                            <input type="text" class="form-control" aria-label="Small"
                                                   name="pixel_size"
                                                   aria-describedby="inputGroup-sizing-sm">
                                        </div>
                                        <div class="input-group input-group-sm mb-3">
                                            <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Sensor</span>
                                            </div>
                                            <input type="text"
                                                   name="sensor"
                                                   class="form-control" aria-label="Small"
                                                   aria-describedby="inputGroup-sizing-sm">
                                        </div>
                                        <div class="input-group input-group-sm mb-3">
                                            <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Type</span>
                                            </div>
                                            <input type="text"
                                                   name="type"
                                                   class="form-control" aria-label="Small"
                                                   aria-describedby="inputGroup-sizing-sm">
                                        </div>
                                        <div class="input-group input-group-sm mb-3">
                                            <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Aperture</span>
                                            </div>
                                            <input type="text"
                                                   name="aperture"
                                                   class="form-control" aria-label="Small"
                                                   aria-describedby="inputGroup-sizing-sm">
                                        </div>

                                        <div class="input-group input-group-sm mb-3">
                                            <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">ISO</span>
                                            </div>
                                            <input type="text"
                                                   name="iso"
                                                   class="form-control" aria-label="Small"
                                                   aria-describedby="inputGroup-sizing-sm">
                                        </div>
                                        <div class="input-group input-group-sm mb-3">
                                            <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Flash</span>
                                            </div>
                                            <input type="text"
                                                   name="flash"
                                                   class="form-control" aria-label="Small"
                                                   aria-describedby="inputGroup-sizing-sm">
                                        </div>
                                        <div class="input-group input-group-sm mb-3">
                                            <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Features</span>
                                            </div>
                                            <input type="text"
                                                   name="features"
                                                   class="form-control" aria-label="Small"
                                                   aria-describedby="inputGroup-sizing-sm">
                                        </div>
                                        <div class="input-group input-group-sm mb-3">
                                            <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Slow motion video</span>
                                            </div>
                                            <input type="text"
                                                   name="slow_motion_video"
                                                   class="form-control" aria-label="Small"
                                                   aria-describedby="inputGroup-sizing-sm">
                                        </div>

                                    </div>


                                </div>

                                <h5>Performance Hardware</h5>

                                <div class="row">
                                    <div class="col">
                                        <div id="processor">
                                            <h6>Processor</h6>

                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Model</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="model"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">CPU</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="cpu"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Type</span>
                                                </div>
                                                <input type="text"
                                                       name="type"
                                                       class="form-control" aria-label="Small"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Frequency</span>
                                                </div>
                                                <input type="text"
                                                       name="frequency"
                                                       class="form-control" aria-label="Small"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">64bits</span>
                                                </div>
                                                <input type="text"
                                                       name="64bits"
                                                       class="form-control" aria-label="Small"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                        </div>
                                        <div id="battery">
                                            <h6>Battery</h6>

                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Capacity</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="capacity"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Type</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="type"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Others</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="others"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>

                                        </div>

                                    </div>
                                    <div class="col">
                                        <div id="graphics">
                                            <h6>Graphics</h6>

                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">GPU</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="gpu"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                        </div>
                                        <div id="ram">
                                            <h6>RAM</h6>

                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">RAM</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="ram"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                        </div>
                                        <div id="antutu">
                                            <h6>Antutu</h6>

                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Score</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="score"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                        </div>
                                        <div id="others">
                                            <h6>Others</h6>

                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Notifications LED</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="notifications_led"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                        </div>
                                        <div id="software">
                                            <h6>Software</h6>

                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">OC</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="oc"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                        </div>
                                        <div id="security">
                                            <h6>Security</h6>

                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Fingerprint</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="fingerprint"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col">
                                        <div id="storage">
                                            <h6>Storage</h6>

                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Capacity</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="capacity"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Equivalent</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="equivalent"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">SD slot</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="sd_slot"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                        </div>
                                        <div id="sensors">
                                            <h6>Sensors</h6>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Fingerprint</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="fingerprint"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Accelerometer</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="accelerometer"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Gyroscope</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="gyroscope"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Hall</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="hall"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Light sensor</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="light_sensor"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Proximity</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="proximity"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                        </div>

                                    </div>


                                </div>

                                <h5>Connectivity</h5>
                                <div class="row">
                                    <div class="col">
                                        <div id="bands">
                                            <h6>Bands</h6>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">4G LTE</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="4g_lte"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">3G</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="3g"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">2G</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="2g"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                        </div>
                                        <div id="sim_card">
                                            <h6>SIM Card</h6>

                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Type</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="type"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                        </div>
                                        <div id="wifi">
                                            <h6>Wi-Fi</h6>

                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Type</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="type"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                        </div>
                                        <div id="sar">
                                            <h6>SAR</h6>

                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">European Measurement</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="european_measurement"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div id="usb">
                                            <h6>USB</h6>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Charging</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="charging"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>

                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Host</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="host"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>

                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Mass Storage</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="mass_storage"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">USB OTG</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="usb_otg"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>

                                        </div>
                                        <div id="bluetooth">
                                            <h6>Bluetooth</h6>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Version</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="version"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>

                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Profiles</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="profiles"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col">
                                        <div id="navigation">
                                            <h6>Navigation</h6>

                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Supports</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="supports"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                        </div>

                                        <div id="connectivity_others">
                                            <h6>Others</h6>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Audio Jack</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="audio_jack"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Radio FM</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="radio_fm"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Computer sync</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="computer_sync"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">Infrared</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="infrared"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">OTA sync</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="ota_sync"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-sm">VoLTE</span>
                                                </div>
                                                <input type="text" class="form-control" aria-label="Small"
                                                       name="vo_lte"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <h5>Photos</h5>
                                <div class="row">
                                    <h5>
                                        <button type="button" data-type="image" data-url="{{ action('UploadController@upload') }}" id="upload_images" class="upload btn btn-warning">Upload image
                                        </button>
                                    </h5>
                                    <input id='camera_image' type="file" name="image[]" hidden>
                                    <input id="images" type="text" name="images" hidden>
                                </div>
                                <div id="photos" class="row"></div>


                                <h5>Reviews video</h5>
                                <div class="row">
                                    <h5>
                                        <button type="button" data-type="video" data-url="{{ action('UploadController@upload') }}" id="upload_video" class=" upload btn btn-warning">Upload video
                                        </button>
                                    </h5>
                                    <input id='video' type="file" name="videos" hidden>
                                    <input id="video_input" type="text" name="videos" hidden>
                                </div>
                                <div id="videos" class="row"></div>


                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="settings" role="tabpanel" aria-labelledby="pills-profile-tab">


            <h3>Quick specifications</h3>


            <div id='message' class="alert alert-success alert-dismissible fade show" role="alert"
                 style="display: none">
                <strong>Success</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form id="save_fields" method="post" action="{{ action('FieldController@store') }}" accept-charset="UTF-8"
                  enctype="multipart/form-data">


                <div class="inputs">

                    @foreach($fields as $field)
                        <div class="row field" data-field="{{$field->id}}">
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="title[]" placeholder="Field title"
                                       value="{{$field->title}}">
                            </div>
                            <div class="col-sm-5">
                                <input type="file" class="form-control" name="icon[]" placeholder="Field icon"
                                       value="{{$field->icon}}">
                            </div>
                            <div class="col-sm-1">
                                <button type="button"
                                        data-url="{{ action('FieldController@destroy',['id'=>'']) }}"

                                        class="delete_field btn btn-dark">Delete
                                </button>
                            </div>
                        </div>

                    @endforeach
                </div>
                <input name="_token" type="hidden" value="{{ csrf_token() }}"/>


                <button id="add_field" data-url="{{ action('FieldController@create') }}" type="button"
                        class="btn btn-danger">Add field
                </button>

                <button type="submit" class="btn btn-primary">Save</button>

            </form>

            <div id='field_template' class="row field" style="display: none">
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="title[]" placeholder="Field title">
                </div>
                <div class="col-sm-5">
                    <input type="file" class="form-control" name="icon[]" placeholder="Field icon">
                </div>
                <div class="col-sm-1">
                    <button type="button"
                            data-url="{{ action('FieldController@destroy',['id'=>'']) }}"
                            class="delete_field btn btn-dark">Delete
                    </button>
                </div>
            </div>
        </div>
    </div>








































@endsection