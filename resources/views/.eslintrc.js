const isDev = process.env.NODE_ENV === 'development';

let eslintrc = {
    extends: ["eslint:recommended"],
    parserOptions: {
        "ecmaVersion": 6,
        "sourceType": "module"
    },
    env: {
        "browser": true,
        "node": true
    },
    rules: {
        "no-undef": 0
    }
};

if (isDev) {
    eslintrc.rules = {
    	"no-console": 0,
		"no-unused-vars": 0,
        "no-undef": 0
    }
}

module.exports = eslintrc;