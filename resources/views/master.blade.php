<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Feedback App</title>
    <meta name="description" content="">
    <meta name="robots" content="">
    <link rel="dns-prefetch" href="http://www.google-analytics.com">
    <link rel="author" href="humans.txt">
    <meta name="format-detection" content="telephone=no">
    <meta name="cmsmagazine" content="a39ef97fd1d4cf6d3e103f0ff48ea4f6">
    <meta name="it-rating" content="it-rat-c2fb848de4ce4a9d8631b1558f63157d">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="shopifyAppType" content="{{ env('SHOPIFY_APP_TYPE') }}">
    <meta name="shopifyAppKey" content="{{ env('SHOPIFY_API_KEY') }}">
    <meta name="domain" content="{{ !empty($shop_domain) ? $shop_domain : '' }}">
    {{--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">--}}
    <script src="{{ asset('assets/js/app.bundle.js') }}"></script>

</head>
<body>

<div class="container">
    @yield('content')
</div>

</body>
</html>