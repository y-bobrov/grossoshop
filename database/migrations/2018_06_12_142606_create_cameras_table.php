<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCamerasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
*/
    public function up()
    {
        Schema::create('cameras', function (Blueprint $table) {
            $table->increments('id');
            $table->char('resolution',100);
            $table->char('pixel_size',100);
            $table->char('sensor',100);
            $table->char('type',100);
            $table->char('aperture',100);
            $table->char('iso',100);
            $table->char('flash',100);
            $table->char('features',100);
            $table->char('slow_motion_video',100);

            $table->unsignedBigInteger('product_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cameras');
    }
}
