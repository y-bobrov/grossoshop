<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignStructuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('design_structures', function (Blueprint $table) {
            $table->increments('id');
            $table->char('size',100);
            $table->char('aspect_ratio',100);
            $table->char('weight',100);
            $table->char('materials',100);
            $table->char('colors',100);

            $table->unsignedBigInteger('product_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('design_structures');
    }
}
