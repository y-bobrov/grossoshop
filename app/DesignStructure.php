<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DesignStructure extends Model
{
    protected $fillable = ['size', 'aspect_ratio','weight','materials','colors','product_id'];

    public $timestamps = false;

}
