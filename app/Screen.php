<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Screen extends Model
{
    protected $fillable = ['diagonal', 'type','resolution','density','others','product_id'];

    public $timestamps = false;

}
