<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Camera extends Model
{
    protected $fillable = ['resolution', 'pixel_size', 'sensor', 'type', 'aperture', 'iso', 'flash', 'features', 'slow_motion_video', 'product_id'];

    public $timestamps = false;
}
