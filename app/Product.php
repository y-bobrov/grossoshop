<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['product_id', 'title', 'meta'];

    protected $primaryKey = 'product_id';


    public function fields()
    {
        return $this->belongsToMany(Fields::class, 'products_fields', 'product_id', 'field_id')
            ->withPivot('value');
    }


    public function designStructure()
    {
        return $this->hasOne(DesignStructure::class, 'product_id');
    }

    public function screen()
    {
        return $this->hasOne(Screen::class, 'product_id');
    }

    public function camera()
    {
        return $this->hasOne(Camera::class, 'product_id');
    }

    public function photos()
    {
        return $this->hasMany(Photo::class, 'product_id');

    }

    public function videos()
    {
        return $this->hasMany(Video::class, 'product_id');

    }
}
