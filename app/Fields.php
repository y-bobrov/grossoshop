<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fields extends Model
{
    protected $fillable = ['title', 'icon'];

    public $timestamps = false;


    public function products()
    {
        return $this->belongsToMany(Product::class, 'products_fields', 'field_id', 'product_id');
    }
}
