<?php

namespace App\Http\Controllers;

use App\Photo;
use App\Video;
use Illuminate\Http\Request;

class UploadController extends Controller
{

    public function upload(Request $request)
    {

        if ($request->hasFile('camera_image')) {
            $file = $request->file('camera_image');
            $filename = str_random(10) . '.' . $file->getClientOriginalExtension();
            $file->storeAs('public', $filename);

            if($request->get('type')=='image'){
                $file = Photo::create(['url' => url('storage/' . $filename)]);

            }elseif ($request->get('type')=='video'){
                $file = Video::create(['url' => url('storage/' . $filename)]);

            }
            return $file;
        }

        else return false;
    }
}
