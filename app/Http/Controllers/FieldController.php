<?php

namespace App\Http\Controllers;

use App\Fields;
use Illuminate\Http\Request;

class FieldController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Fields::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Fields::count() + 1;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $titles = $request->input('title');
        $files = $request->allFiles();
        foreach ($titles as $key => $title) {

            $field = Fields::firstOrNew(['title' => $title]);
            //$file = $files['icon'][$key];
            if (array_key_exists($key, $files['icon'])) {
                $filename = str_random(10) . '.' . $files['icon'][$key]->getClientOriginalExtension();
                $files['icon'][$key]->storeAs('public', $filename);
                $field->icon = url('storage/' . $filename);

            }
            $field->save();
        }
        $response = ['success' => true];
        return json_encode($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = ['success' => false];
        //var_dump($id);
        if ($field = Fields::destroy($id)) {

            $response['success'] = true;

        }
        return json_encode($response);
    }
}
