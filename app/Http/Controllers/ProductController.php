<?php

namespace App\Http\Controllers;

use App\QuickSpecifications;
use App\Camera;
use App\Fields;
use App\Video;
use App\Photo;
use App\Product;
use App\Screen;
use App\Shop;
use Illuminate\Http\Request;
use App\Services\ShopifyService;

class ProductController extends Controller
{
    private $shopifyClient;

    public function __construct(Request $request)
    {
        $shop = Shop::first();
       // $shop = Shop::where(['domain' => '$request'])->first();

        $this->shopifyClient = ShopifyService::makeShopifyClient($shop->domain);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $products = $this->shopifyClient->call([
                'URL' => 'products.json',
            ]);


        } catch (\Exception $e) {
            $products = $e->getMessage();
        }
        // dd($products);

        return view('pages.index', ['products' => $products->products,
            'fields' => Fields::all()
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

//        try {
//            $product = $this->shopifyClient->call([
//                'URL' => "products/#$id.json",
//            ]);
//
//
//        } catch (\Exception $e) {
//            $product = $e->getMessage();
//        }
        $product = Product::firstOrNew(['product_id' => $id]);

        if (!$product->meta) {
            $path = storage_path() . "/structure.json";
            //dd($path);
            $product->meta = file_get_contents($path);
            $product->save();

        }
        $response = (object)[
            'quick_specifications' => $product->fields()->get(),
            'meta' => json_decode($product->meta),
            'photos' => $product->photos()->get(),
            'videos' => $product->videos()->get()
        ];
        return response(json_encode($response))->header('Content-Type', 'application/json');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $response = ['success' => true];
        $output = [];
        $product = Product::findOrFail($id);
        $product->meta = json_encode($request->all());
        $product->save();

        $imagesId = explode(',', $request->get("images"));
        $videosId = explode(',', $request->get('videos'));
        foreach ($imagesId as $imageId) {
            Photo::where('id', $imageId)->update(['product_id' => $product->product_id]);
        }

        foreach ($videosId as $videoId) {
            Video::where('id', $videoId)->update(['product_id' => $product->product_id]);
        }
        $quickSpecifications = $request->get('quick_specifications');

        $fields = Fields::all();


        foreach ($fields as $field) {

            QuickSpecifications::create([
                'product_id' => $product->product_id,
                'field_id' => $field->id,
                'value' => $quickSpecifications[$field->title]
            ]);
            $output['quick_specifications'][] = [
                'icon' => $field->icon,
                'value' => $quickSpecifications[$field->title]
            ];
        }

        $metafildsData = json_decode($product->meta, true);
        $metafildsData['quick_specifications'] = $output['quick_specifications'];
        $metafildsData['images'] = $product->photos()->get()->toArray();
        $metafildsData['videos'] = $product->videos()->get()->toArray();
        unset($metafildsData['quick_spec']);
        unset($metafildsData['undefined']);

        $meta = [
            "namespace" => "inventory",
            "key" => "meta",
            "value" => json_encode($metafildsData),
            "value_type" => "string"
        ];


        $products = $this->shopifyClient->call([
            'METHOD' => 'POST',
            'URL' => "products/$product->product_id/metafields.json",
            'FAILONERROR' => false,
            'DATA' => [
                'metafield' => $meta
            ]
        ]);
        $products = $this->shopifyClient->call([
            'METHOD' => 'GET',
            'URL' => "products/$product->product_id/metafields.json",
            'FAILONERROR' => false
        ]);
        return response(json_encode($response))->header('Content-Type', 'application/json');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
