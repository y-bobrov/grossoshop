<?php

namespace App\Providers;

use App\DesignStructure;
use App\Camera;
use App\Product;
use App\Fields;
use App\QuickSpecifications;
use App\Screen;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Product::creating(function ($product) {

            $fields = Fields::all();


            foreach ($fields as $field) {

                QuickSpecifications::create([
                    'product_id' => $product->product_id,
                    'field_id' => $field->id,
                    'value' => "test_value_$product->product_id"
                ]);


            }

            DesignStructure::create([
                'size' => 'qwe',
                'aspect_ratio' => 'qwe',
                'weight' => 'qwer',
                'materials' => 'qwer',
                'colors' => 'qwer',
                'product_id' => $product->product_id
            ]);

            Screen::create([
                'diagonal' => 'fewqefq',
                'type' => 'feq',
                'resolution' => 'fqew',
                'density' => 'feq',
                'others' => 'fqew',
                'product_id' => $product->product_id
            ]);

            Camera::create(["resolution" => "wef",
                "pixel_size" => 'wefwef',
                "sensor" => 'wefwe',
                "type" => 'wefew',
                "aperture" => 'wefwef',
                "iso" => 'wefwef',
                "flash" => 'wefwef',
                "features" => 'wefwef',
                "slow_motion_video" => 'wefewf',
                "product_id" => $product->product_id]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
