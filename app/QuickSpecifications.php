<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuickSpecifications extends Model
{
    protected $table = 'products_fields';
    protected $fillable = ['product_id', 'field_id', 'value'];
    public $timestamps = false;


}
